import React, {useState} from 'react';
import {navCatagories} from '../../Data/NavData';
import {navItems} from '../../Data/NavItems';
import styles from './navigation.module.scss';

const Navigation = () => {
  const [subMenuOpen, setSubMenu] = useState(false);
  const [setIndex, preIndex] = useState();


  const showDropdown = (e) => {
    setSubMenu(true);
    preIndex(e.currentTarget.dataset.index);
  }
 
  const hideDropdown = () => {
    setSubMenu(false);
  }
  
  
  const items = navItems.map((item, i) => {
    return ( 
      <span key={i} data-index={i} onMouseEnter={showDropdown} className={styles.item}>
        <a className={styles.link} href={item}>{item}</a>
      </span>
    );
  });

  const subItems = navCatagories.map((data, i)  => {
    return (
      data.children_data.map((child, index) => {
        return (
          <span key={index} className={styles.item}>
              <a className={styles.link} href={child.name}>{child.name}</a>
          </span>
        );
      })
    )
  })


  return (
    <div className={styles.content}>
      <header  className={styles.header}>
        <div className={styles.nav}>
          {items}
          
          {subMenuOpen && subItems[setIndex] && ( <div onMouseLeave={hideDropdown} className={styles.dropdown}>
              {subItems[setIndex]}
            </div>
          )}
        </div>
      </header>
    </div>
  );
};

export default Navigation;
