export const navItems = [
    "Latest",
    "Brands",
    "Clothing",
    "Footwear",
    "Accessories",
    "Lifestyle",
    "Gifts",
    "Sale"
];

export default navItems;